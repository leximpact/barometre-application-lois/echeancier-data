# Données échéancier

_Données constituées à partir de l'open data de Légifrance et de l'Assemblée nationale, plus une classification manuelle des mesures figurant dans les échéanciers des lois_

Ces données sont générées et utilisées par le [Baromètre de l'application des lois (aussi appelé Échéancier)](https://git.leximpact.dev/leximpact/barometre-application-lois/echeancier).
